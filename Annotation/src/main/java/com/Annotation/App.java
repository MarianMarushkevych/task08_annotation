package com.Annotation;


public class App 
{
    public static void main( String[] args )
    {
        IPhone iphone = new IPhone();
        Class iphone1 = iphone.getClass();
        Phone iphone2 = (Phone) iphone1.getAnnotation(Phone.class);
        System.out.println(iphone2.mm());

        Apple apple = new Apple();
        Class apple1 = apple.getClass();
        Phone apple2 = (Phone) apple1.getAnnotation(Phone.class);
        System.out.println(apple2.mm());
    }
}
